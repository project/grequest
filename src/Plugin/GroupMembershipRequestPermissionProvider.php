<?php

namespace Drupal\grequest\Plugin;

use Drupal\group\Plugin\GroupContentPermissionProvider;

/**
 * Provides group permissions for group content entities.
 */
class GroupMembershipRequestPermissionProvider extends GroupContentPermissionProvider {

  /**
   * {@inheritdoc}
   */
  public function getEntityCreatePermission() {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityDeletePermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityViewPermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityUpdatePermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelationViewPermission($scope = 'any') {
    return "view $scope $this->pluginId content";
  }

  /**
   * {@inheritdoc}
   */
  public function getRelationUpdatePermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelationDeletePermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelationCreatePermission($scope = 'any') {
    // Handled by the admin permission.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPermissions() {
    $permissions = parent::buildPermissions();

    // Update the title to make user friendly.
    $permissions[$this->getAdminPermission()]['title'] = 'Administer membership requests';

    $permissions[$this->getRequestGroupMembershipPermission()] = [
      'title' => 'Request group membership',
      'allowed for' => ['outsider'],
    ];

    $permissions[$this->getRelationViewPermission()]['title'] = 'View any membership requests';
    $permissions[$this->getRelationViewPermission('own')]['title'] = 'View own membership requests';

    return $permissions;
  }

  /**
   * Get request membership permission.
   *
   * @return string
   *   Permission name.
   */
  public function getRequestGroupMembershipPermission() {
    return 'request group membership';
  }

}
