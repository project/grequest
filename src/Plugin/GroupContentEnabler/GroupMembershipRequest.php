<?php

namespace Drupal\grequest\Plugin\GroupContentEnabler;

use Drupal\Core\ProxyClass\Config\ConfigInstaller;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Plugin\GroupContentEnablerBase;
use Drupal\group\Plugin\GroupContentEnablerManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a content enabler for users.
 *
 * @GroupContentEnabler(
 *   id = "group_membership_request",
 *   label = @Translation("Group membership request"),
 *   description = @Translation("Adds users as requesters for the group."),
 *   entity_type_id = "user",
 *   pretty_path_key = "request",
 *   reference_label = @Translation("Username"),
 *   reference_description = @Translation("The name of the user you want to make a member"),
 *   handlers = {
 *     "permission_provider" = "Drupal\grequest\Plugin\GroupMembershipRequestPermissionProvider",
 *   },
 *   admin_permission = "administer membership requests"
 * )
 */
class GroupMembershipRequest extends GroupContentEnablerBase implements ContainerFactoryPluginInterface {

  /**
   * Transition id for approval.
   */
  const TRANSITION_APPROVE = 'approve';

  /**
   * Transition id for approval.
   */
  const TRANSITION_REJECT = 'reject';

  /**
   * Transition id for creation.
   */
  const TRANSITION_CREATE = 'create';

  /**
   * Status field.
   */
  const STATUS_FIELD = 'grequest_status';

  /**
   * Request created by default with new status.
   */
  const REQUEST_NEW = 'new';

  /**
   * Request created and waiting for administrator's response.
   */
  const REQUEST_PENDING = 'pending';

  /**
   * Request is approved by administrator.
   */
  const REQUEST_APPROVED = 'approved';

  /**
   * Request is rejected by administrator.
   */
  const REQUEST_REJECTED = 'rejected';

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The permission provider.
   *
   * @var \Drupal\group\Plugin\GroupContentPermissionProviderInterface
   */
  protected $permissionProvider;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config installer.
   *
   * @var \Drupal\Core\ProxyClass\Config\ConfigInstaller
   */
  protected $configInstaller;

  /**
   * Group membersip request group enabler.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\group\Plugin\GroupContentEnablerManagerInterface $group_content_enabler_manager
   *   The group content enabler manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\ProxyClass\Config\ConfigInstaller $config_installer
   *   Config installer.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountInterface $current_user,
    GroupContentEnablerManagerInterface $group_content_enabler_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigInstaller $config_installer
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentUser = $current_user;
    $this->permissionProvider = $group_content_enabler_manager->getPermissionProvider($this->getPluginId());
    $this->entityTypeManager = $entity_type_manager;
    $this->configInstaller = $config_installer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('plugin.manager.group_content_enabler'),
      $container->get('entity_type.manager'),
      $container->get('config.installer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupOperations(GroupInterface $group) {
    $operations = [];

    $entity_instances = $group->getContentByEntityId($this->getPluginId(), $this->currentUser->id());
    $url = $group->toUrl('group-request-membership');
    if ($url->access($this->currentUser) && count($entity_instances) == 0) {
      $operations['group-request-membership'] = [
        'title' => $this->t('Request group membership'),
        'url' => $url,
        'weight' => 99,
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function createAccess(GroupInterface $group, AccountInterface $account) {
    return GroupAccessResult::allowedIfHasGroupPermission($group, $account, $this->permissionProvider->getAdminPermission());
  }

  /**
   * {@inheritdoc}
   */
  protected function viewAccess(GroupContentInterface $group_content, AccountInterface $account) {

    $access = GroupAccessResult::neutral();
    $group = $group_content->getGroup();

    if ($group->hasPermission($this->permissionProvider->getAdminPermission(), $account)) {
      $access = GroupAccessResult::allowed();
    }
    elseif ($group->hasPermission($this->permissionProvider->getRelationViewPermission(), $account)) {
      $access = GroupAccessResult::allowed();
    }
    elseif ($group->hasPermission($this->permissionProvider->getRelationViewPermission('own'), $account) && $group_content->getOwnerId() == $account->id()) {
      $access = GroupAccessResult::allowed();
    }

    return $access->addCacheContexts(['user.group_permissions']);
  }

  /**
   * {@inheritdoc}
   */
  protected function updateAccess(GroupContentInterface $group_content, AccountInterface $account) {
    return GroupAccessResult::allowedIfHasGroupPermission($group_content->getGroup(), $account, $this->permissionProvider->getAdminPermission());
  }

  /**
   * {@inheritdoc}
   */
  protected function deleteAccess(GroupContentInterface $group_content, AccountInterface $account) {
    return GroupAccessResult::allowedIfHasGroupPermission($group_content->getGroup(), $account, $this->permissionProvider->getAdminPermission());
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityReferenceSettings() {
    $settings = parent::getEntityReferenceSettings();
    $settings['handler_settings']['include_anonymous'] = FALSE;
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function postInstall() {
    if ($this->configInstaller->isSyncing()) {
      return;
    }

    $field_config_storage = $this->entityTypeManager->getStorage('field_config');
    $field_storage_config_storage = $this->entityTypeManager->getStorage('field_storage_config');
    $entity_view_display_storage = $this->entityTypeManager->getStorage('entity_view_display');

    $group_content_type_id = $this->getContentTypeConfigId();

    // Add Status field.
    $field_config_storage->create([
      'field_storage' => $field_storage_config_storage->load('group_content.' . GroupMembershipRequest::STATUS_FIELD),
      'bundle' => $group_content_type_id,
      'label' => $this->t('Request status'),
      'required' => TRUE,
      'settings' => [
        'workflow' => 'request',
        'workflow_callback' => '',
      ],
    ])->save();

    // Add "Updated by" field, to save reference to
    // user who approved/denied request.
    $field_config_storage->create([
      'field_storage' => $field_storage_config_storage->load('group_content.grequest_updated_by'),
      'bundle' => $group_content_type_id,
      'label' => $this->t('Approved/Rejected by'),
      'settings' => [
        'handler' => 'default',
        'target_bundles' => NULL,
      ],
    ])->save();

    // Build the 'default' display ID for both the entity form and view mode.
    $default_display_id = "group_content.$group_content_type_id.default";
    // Build or retrieve the 'default' view mode.
    if (!$view_display = $entity_view_display_storage->load($default_display_id)) {
      $view_display = $entity_view_display_storage->create([
        'targetEntityType' => 'group_content',
        'bundle' => $group_content_type_id,
        'mode' => 'default',
        'status' => TRUE,
      ]);
    }

    // Assign display settings for the 'default' view mode.
    $view_display
      ->setComponent('grequest_status', [
        'type' => 'list_default',
      ])
      ->setComponent('grequest_updated_by', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'settings' => [
          'link' => 1,
        ],
      ])
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['entity_cardinality'] = 1;
    $config['remove_group_membership_request'] = 0;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['remove_group_membership_request'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove a group membership request, when user join the group.'),
      '#default_value' => $this->getConfiguration()['remove_group_membership_request'] ?? FALSE,
    ];

    // Disable the entity cardinality field as the functionality of this module
    // relies on a cardinality of 1. We don't just hide it, though, to keep a UI
    // that's consistent with other content enabler plugins.
    $info = $this->t("This field has been disabled by the plugin to guarantee the functionality that's expected of it.");
    $form['entity_cardinality']['#disabled'] = TRUE;
    $form['entity_cardinality']['#description'] .= '<br /><em>' . $info . '</em>';

    return $form;
  }

}
