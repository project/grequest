<?php

namespace Drupal\grequest\Plugin\views\field;

use Drupal\grequest\Plugin\GroupContentEnabler\GroupMembershipRequest;
use Drupal\views\Plugin\views\field\EntityLink;
use Drupal\views\ResultRow;

/**
 * Field handler to present an entity link.
 */
abstract class MembershipEntityLink extends EntityLink {

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row) {
    $plugin_id = 'group_membership_request';
    /** @var \Drupal\group\Entity\GroupContent $group_content */
    $group_content = $row->_entity;
    $group = $group_content->getGroup();
    $link = NULL;

    // Check if plugin exists.
    if (!$group->getGroupType()->hasContentPlugin($plugin_id)) {
      return $link;
    }
    // Check if current group content is type of group_membership_request.
    if ($group_content->getContentPlugin()->getPluginId() !== $plugin_id) {
      return $link;
    }

    $user = $group_content->getEntity();

    if (!empty($group->getMember($user))) {
      $link = $this->t('Already member');
    }
    elseif ($group_content->get(GroupMembershipRequest::STATUS_FIELD)->value === GroupMembershipRequest::REQUEST_PENDING && $group->hasPermission('administer membership requests', $this->currentUser)) {
      $this->options['alter']['query'] = $this->getDestinationArray();
      $link = parent::renderLink($row);
    }

    return $link;
  }

}
