<?php

namespace Drupal\Tests\grequest\Kernel;

use Drupal\grequest\Plugin\GroupContentEnabler\GroupMembershipRequest;
use Drupal\group\Entity\GroupContent;
use Drupal\group\GroupMembership;
use Drupal\Tests\group\Kernel\GroupKernelTestBase;

/**
 * Tests the general behavior of group entities.
 *
 * @coversDefaultClass \Drupal\group\Entity\Group
 * @group group
 */
class GroupMembershipRequestTest extends GroupKernelTestBase {

  /**
   * Membership request manager.
   *
   * @var \Drupal\grequest\MembershipRequestManager
   */
  protected $membershipRequestManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The group we will use to test methods on.
   *
   * @var \Drupal\group\Entity\Group
   */
  protected $group;

  /**
   * The group content type for group membership request.
   *
   * @var \Drupal\group\Entity\GroupContentTypeInterface
   */
  protected $groupContentType;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'grequest',
    'state_machine',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');

    $this->installConfig([
      'grequest',
      'state_machine',
    ]);

    $this->membershipRequestManager = $this->container->get('grequest.membership_request_manager');
    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->group = $this->createGroup();

    $config = [
      'group_cardinality' => 0,
      'entity_cardinality' => 1,
      'remove_group_membership_request' => 0,
    ];
    // Enable group membership request group content plugin.
    $this->groupContentType = $this->entityTypeManager->getStorage('group_content_type')->createFromPlugin($this->group->getGroupType(), 'group_membership_request', $config);
    $this->groupContentType->save();
  }

  /**
   * Test the creation of the membership request when user is the member.
   */
  public function testAddRequestForMember() {
    $account = $this->createUser();
    $this->group->addMember($account);

    $this->expectExceptionMessage('This user is already a member of the group');
    $this->membershipRequestManager->create($this->group, $account);
  }

  /**
   * Test approval.
   */
  public function testRequestApproval() {
    $account = $this->createUser();
    $group_membership_request = $this->createRequestMembership($account);
    $this->membershipRequestManager->approve($group_membership_request);
    $this->assertEquals($group_membership_request->get(GroupMembershipRequest::STATUS_FIELD)->value, GroupMembershipRequest::REQUEST_APPROVED);
  }

  /**
   * Test rejection.
   */
  public function testRequestRejection() {
    $account = $this->createUser();
    $group_membership_request = $this->createRequestMembership($account);
    $this->membershipRequestManager->reject($group_membership_request);
    $this->assertEquals($group_membership_request->get(GroupMembershipRequest::STATUS_FIELD)->value, GroupMembershipRequest::REQUEST_REJECTED);
  }

  /**
   * Test wrong status update workflow.
   */
  public function testWrongRequestWorkflow() {
    $account = $this->createUser();
    $group_membership_request = $this->createRequestMembership($account);
    $this->membershipRequestManager->approve($group_membership_request);
    $this->assertEquals($group_membership_request->get(GroupMembershipRequest::STATUS_FIELD)->value, GroupMembershipRequest::REQUEST_APPROVED);
    $this->expectExceptionMessage('Transition "reject" is not allowed.');
    $this->membershipRequestManager->reject($group_membership_request);
  }

  /**
   * Create group membership request.
   *
   * @return \Drupal\group\Entity\GroupContentInterface
   *   Group membership request.
   */
  public function createRequestMembership($account) {
    $group_membership_request = $this->membershipRequestManager->create($this->group, $account);
    $group_membership_request->save();

    $this->assertEquals($group_membership_request->get(GroupMembershipRequest::STATUS_FIELD)->value, GroupMembershipRequest::REQUEST_PENDING);

    return $group_membership_request;
  }

  /**
   * Test approval with roles.
   */
  public function testApprovalWithRoles() {
    $account = $this->createUser();
    $role_name = 'default-custom';

    $group_membership_request = $this->createRequestMembership($account);
    $this->membershipRequestManager->approve($group_membership_request, [$role_name]);

    $group_membership = $this->group->getMember($account);
    $this->assertTrue($group_membership instanceof GroupMembership, 'Group membership has been successfully created.');

    $this->assertTrue(in_array($role_name, array_keys($group_membership->getRoles())), 'Role has been found');
  }

  /**
   * Test deletion of group membership request after user deletion.
   */
  public function testUserDeletion() {
    $account = $this->createUser();
    $this->createRequestMembership($account);

    $this->entityTypeManager->getStorage('user')->delete([$account]);

    $membership_request = $this->membershipRequestManager->getMembershipRequest($account, $this->group);
    $this->assertNull($membership_request);
  }

  /**
   * Test deletion of group membership request after group membership deletion.
   */
  public function testGroupMembershipDeletion() {
    $account = $this->createUser();

    $group_membership_request = $this->createRequestMembership($account);
    $this->membershipRequestManager->approve($group_membership_request);

    $this->group->removeMember($account);

    $membership_request = $this->membershipRequestManager->getMembershipRequest($account, $this->group);
    $this->assertNull($membership_request);
  }

  /**
   * Test a wrong group content.
   */
  public function testWrongGroupContent() {
    $account = $this->createUser();
    $group_membership_group_content = GroupContent::create([
      'type' => $this->group
        ->getGroupType()
        ->getContentPlugin('group_membership')
        ->getContentTypeConfigId(),
      'gid' => $this->group->id(),
      'entity_id' => $account->id(),
    ]);

    $this->expectExceptionMessage('Only group content of "Group membership request" is allowed.');
    $this->membershipRequestManager->approve($group_membership_group_content);
  }

  /**
   * Test group membership removal with disabled settings.
   */
  public function testRequestRemovalWithDisabledSettings() {
    $account = $this->createUser();

    // Add first group membership request.
    $group_membership_request = $this->membershipRequestManager->create($this->group, $account);
    $group_membership_request->save();

    // Add the user as member.
    $this->group->addMember($account);

    // Since removal is disabled we should see find group membership request.
    $group_membership_request = $this->membershipRequestManager->getMembershipRequest($account, $this->group);
    $this->assertNotNull($group_membership_request);
  }

  /**
   * Test group membership removal with enabled settings.
   */
  public function testRequestRemovalWithEnabledSettings() {
    $config = [
      'group_cardinality' => 0,
      'entity_cardinality' => 1,
      'remove_group_membership_request' => 1,
    ];
    $this->groupContentType->updateContentPlugin($config);
    $account = $this->createUser();

    // Add first group membership request.
    $group_membership_request = $this->membershipRequestManager->create($this->group, $account);
    $group_membership_request->save();

    // Add the user as member.
    $this->group->addMember($account);

    // Since removal is disabled we should see find group membership request.
    $group_membership_request = $this->membershipRequestManager->getMembershipRequest($account, $this->group);
    $this->assertNull($group_membership_request);
  }

}
